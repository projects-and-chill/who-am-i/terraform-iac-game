locals {
  name_suffix = "${var.project_name_short}-${var.environment}${var.event_emitter}"
  required_tags = {
    Project = var.project_name_short
    Environment = var.environment
  }
  tags = merge(var.resources_tags, local.required_tags)

}

provider "aws" {
  region = "us-east-2"
}

terraform {
  backend "s3" {
    bucket = "terraform-state-bucket-5hr6tshr6t8h4"
    key    = "terraform/prod/terraform.tfstate"
    region = "us-east-2"
    encrypt = true
  }
}

module "vpc" {
  source = "./../../modules/vpc"
  vpc_cidr_block = var.vpc_cidr_block
  resources_name_suffix = local.name_suffix
  resources_tags = local.tags
}

module "sg_elb" {
  source = "../../modules/security_group"
  vpc_id = module.vpc.vpc_id
  sg_name = "elb-pb-${local.name_suffix}"
  sg_description = "Public security group for elactic load balancer"
  ingress_rules_cidr = [{
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  },{
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  },{
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [var.ssh_allowed_ip]
  },{
    from_port = 80
    to_port = 80
    protocol = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }]
  resources_tags = local.tags
}

module "sg_ec2" {
  source = "../../modules/security_group"
  vpc_id = module.vpc.vpc_id
  sg_name = "ec2-pv-${local.name_suffix}"
  sg_description = "Restricted security group for ec2 instances"
  ingress_rules_cidr = [{
    from_port = 0
    to_port = 65535
    protocol = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  },{
    from_port = -1
    to_port = -1
    protocol = "icmp"
    cidr_blocks = [var.vpc_cidr_block]
  }]
  resources_tags = local.tags
}

module "elb" {
  source = "../../modules/elastic_load_balancer"
  ssl_certificate = var.ssl_certificate
  ressource_name_suffix = local.name_suffix
  vpc_id = module.vpc.vpc_id
  api_name = var.api_name
  elb_security_groups = [module.sg_elb.sg_id]
  elb_subnets = module.vpc.public_subnet_ids
  resources_tags = local.tags
}

module "ecs_cluster"{
  source = "../../modules/ecs_cluster"
  cluster_name = "cluster-${local.name_suffix}"
  vpc_id = module.vpc.vpc_id
  subnets_ids = module.vpc.public_subnet_ids
  ressource_name_suffix = local.name_suffix
  resources_tags = local.tags
  ami_id = var.ami_id
  instance_type = "t2.micro"
  key_name = var.key_pair_name
  sg_ids = [module.sg_ec2.sg_id]
}


resource "aws_cloudwatch_log_group" "cloudwatch_log_group" {
  name = local.name_suffix
  retention_in_days = 5
}

module "ecs_who-am-i_service"{
  source = "../../modules/ecs_service"
  project_name = var.project_name
  node_env = "production"
  api_name = var.api_name
  cluster_id = module.ecs_cluster.cluster_id
  api_service_infos = {
    prefix_name = "api-who-am-i"
    suffix_name = var.environment
    target_group_arn = module.elb.api_target_group_arn
    api_ct = {
      name = "api-who-am-i-container"
      image = var.api_docker_image
      port = 3000
    }
    mongo_ct = {
      name = "mongo-who-am-i-container"
      image = var.mongo_docker_image
      port = 27017
    }
  }

  front_service_infos = {
    prefix_name ="front-who-am-i"
    suffix_name = var.environment
    target_group_arn = module.elb.front_target_group_arn
    front_ct = {
      name = "front-who-am-i-container"
      image = var.front_docker_image
      port = 4200
    }
  }

  log_group_name =  aws_cloudwatch_log_group.cloudwatch_log_group.name
  ressource_name_suffix = local.name_suffix
}
