
variable "project_name" {
  type = string
  description = "project name"
}

variable "project_name_short" {
  type = string
  description = "project names initials or very short project name (for limited name lenght ressources)"
}

variable "event_emitter" {
  type = string
  description = "identity of build trigger"
}


variable "environment" {
  type = string
  description = "environment-name"
}

variable "resources_tags" {
  type = map(string)
  description = "resources default tags"
  default = {}
}

variable "log_group_name" {
  type = string
  description = "name of cloudwatch log group"
}

variable "ami_id" {
  type = string
  description = "ami id for ecs instances"
}


variable "api_name" {
  type = string
  description = "Api name"
}

variable "key_pair_name" {
  type = string
  description = "Name of aws '.pem' key pair for ec2 instances for ssh authentification"
}


variable "ssl_certificate" {
  type = string
  description = "ssl certificate for https"
}


variable "ssh_allowed_ip" {
  type = string
  description = "my public ip address for allow ssh connexions"
  default = "0.0.0.0/32"
  sensitive = true
}


/*---------------- Automation  --------------------*/

variable "vpc_cidr_block" {
  type = string
  description = "vpc cidr block"
  default = "10.0.0.0/16"
}

variable "api_docker_image" {
  type = string
  description = "api docker image"
}

variable "front_docker_image" {
  type = string
  description = "front docker image"
}

variable "mongo_docker_image" {
  type = string
  description = "front docker image"
}

