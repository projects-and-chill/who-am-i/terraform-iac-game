

variable "project_name" {
  type = string
  description = "project name"
}

variable "project_name_short" {
  type = string
  description = "project names initials or very short project name (for limited name lenght ressources)"
}

variable "environment" {
  type = string
  description = "environment name"
}

variable "log_group_name" {
  type = string
  description = "name of cloudwatch log group"
}

variable "ami_id" {
  type = string
  description = "ami id for ecs instances"
}


variable "api_name" {
  type = string
  description = "Api name"
}

variable "key_pair_name" {
  type = string
  description = "Name of aws '.pem' key pair for ec2 instances for ssh authentification"
}


variable "ssh_allowed_ip" {
  type = string
  description = "my public ip address for allow ssh connexions"
  default = "0.0.0.1/32"
  sensitive = true
}

variable "resources_tags" {
  type = map(string)
  description = "resources default tags"
  default = {}
}

variable "ssl_certificate" {
  type = string
  description = "ssl certificate for https"
}


/*---------------- Automation  --------------------*/

variable "event_emitter" {
  type = string
  description = "identity of build trigger"
}

variable "s3_remote_state_path" {
  type = string
  description = "path of remote state file (exemple : 'terraform/staging/ecs/terraform.state')"
}

variable "api_docker_image" {
  type = string
  description = "api docker image"
}

variable "front_docker_image" {
  type = string
  description = "front docker image"
}

variable "mongo_docker_image" {
  type = string
  description = "front docker image"
}


