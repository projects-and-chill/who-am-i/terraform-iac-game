output "cluster_id" {
  description = "Id of cluster"
  value = aws_ecs_cluster.cluster.id
}
