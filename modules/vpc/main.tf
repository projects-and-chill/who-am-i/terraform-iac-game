terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.6.0"
    }
  }
}

data "aws_availability_zones" "current" {}

#---------Creation du VPC et des subnets-----------

resource "aws_vpc" "main" {
  cidr_block = var.vpc_cidr_block
  tags = merge(var.resources_tags, {
    Name = "vpc-${var.resources_name_suffix}"
  })
}

resource "aws_subnet" "public" {
  count = 2
  vpc_id = aws_vpc.main.id
  cidr_block = cidrsubnet(aws_vpc.main.cidr_block, 4, count.index)
  availability_zone = element(data.aws_availability_zones.current.names, count.index)
  map_public_ip_on_launch = true
  tags = merge(var.resources_tags, {
    Name = "public-${count.index}-${var.resources_name_suffix}"
  })
}

#----------Creation de l'internet Gateway du VPC-------------

resource "aws_internet_gateway" "igw" {
  tags = merge(var.resources_tags, {
    Name = "igw-${var.resources_name_suffix}"
  })
}

resource "aws_internet_gateway_attachment" "igw_attachement" {
  internet_gateway_id = aws_internet_gateway.igw.id
  vpc_id = aws_vpc.main.id
}

#----------Creation Tables routage Public et routes-------------

resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.main.id
  tags = merge(var.resources_tags, {
    Name = "rt-public-${var.resources_name_suffix}"
  })
}

resource "aws_route_table_association" "route_table_association" {
  count = length(aws_subnet.public)
  route_table_id = aws_route_table.public_route_table.id
  subnet_id = aws_subnet.public[count.index].id
}

resource "aws_route" "route_ipv4" {
  route_table_id = aws_route_table.public_route_table.id
  gateway_id = aws_internet_gateway.igw.id
  destination_cidr_block = "0.0.0.0/0"
}
