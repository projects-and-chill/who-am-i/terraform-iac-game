output "vpc_id" {
  value = aws_vpc.main.id
  description = "Main vcp id"
}

output "public_subnet_ids" {
  value = aws_subnet.public.*.id
  description = "list of subnet ids"
}
