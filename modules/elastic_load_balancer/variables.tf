variable "api_name" {
  type = string
  description = "name of api"
}

variable "ressource_name_suffix" {
  type = string
  description = "suffix of ressources name"
}

variable "vpc_id" {
  type = string
  description = "vpc id of stack"
}

variable "elb_security_groups" {
  type = list(string)
  description = "ids of security groups to asign at elb"
}

variable "elb_subnets" {
  type = list(string)
  description = "ids of subnets to asign at elb"
}

variable "ssl_certificate" {
  type = string
  description = "ssl certificat arn"
}


/*------------Tags--------------*/
variable "resources_tags" {
  type = map(string)
  description = "common tags for all resources"
  default = {}
}
