# LOCALS COMMANDS :

## Commands : Launch environment


```bash
make init <env> #terraform Init command
```
```bash
make apply <env> #terraform Apply command
```
```bash
make destroy <env> #terraform Destroy command
```

# CICD PIPELINE 

## Centralized deployment (Multi-Project-Pipeline-Segregation) :

### Avantages : 
- Meilleur répartition des fonctionnalités
- Gestion facilité via l'environment UI Gitalb
- Chaque déploiement est naturellement associé a un commit (contrairement à un déploiment via l'API Gitlab )
- Plus facile à maintenir

### Inconvéniants :
- (Avec gitlab-ci) Beaucoup plus compliqué d'attribuer des environment à des projets différent du projet source :
	- Cela nécéssites la création de scripts utilisant l'API Gitlab

## Pipeline interactions :

- On push into **master** do nothing
- On push into **staging** execute environment testing if code changement detected
	- Test staging networking 
	- Test staging network environment + staging cluster environment
	- Test production environement 
- If staging **tests pass successfully** :arrow_right: **Automatically** push into **production**
	- Production branch [create manual :hand:triggerer task](https://gitlab.com/models8/pipeline-model-group/terraform-playbook-pipeline/-/pipelines/561392122) (*on*/*off*) for staging networking environment
 
## Git tag autoincrement strategy :

**FULL RAMIFIED HISTORIC & SLOW INCREMENTATION**
    - a merge generate a "merge commit" with "merge: message"
    - never squash
    - increment ignore "merge" & "squash" commits
    - mainline incrementation

# TERRAFORM ENVIRONMENTS `staging` vs `prod`

## Commons working directory

Image default working directory is `./environments/<env>`

## Commons Environments variables

Aws authentication credentials
- (required)`AWS_ACCESS_KEY_ID`
- (required)`AWS_SECRET_ACCESS_KEY`
- (required)`AWS_DEFAULT_REGION`


##  Zoom in `staging`

#### Global informations 

`staging` environment is decomposed on 2 stacks ( **NETWORKING** and **ECS**)

```text
.environments/staging/
├─ 1-networking-stack/
├─ 2-ecs-stack/
```

#### NETWORKING STACK  



- **networking stack** stack workdir: `./1-networking-stack`
- S3 **backend** `key="file/path"` statically configured (but can be changed with **-backend-config**)
- Variables

|     Variable     |   Required   |   Default   |    Exemple:     |
|:----------------:|:------------:|:-----------:|:---------------:|
| `vpc_cidr_block` | **Optional** | 10.1.0.0/16 | VPCI cidr block |

- Outputs 

```ini
vpc_id="Main vcp id"
public_subnet_ids="list of subnet ids"
elb_security_group_id="security group for load balancers"
ec2_security_group_id="security group for ec2 instances"
```


#### ECS STACK



- **ecs stack** ecs stack workdir: `./2-ecs-stack`
- S3 **backend** `key="file/path"` should be dynamically configured with : 
```bash
terraform init -backend-config="key=terraform/staging/stacks/ecs/<microservice_name>.tfstate"
```
The dynamic state file allows the creation of multiple clusters each having their own terraform state. Thanks to this, each microservice can have its own staging cluster, and independently manage its life cycle.
- Variables

|       Variable       |   Required   |      Default       |              Exemple:              |
|:--------------------:|:------------:|:------------------:|:----------------------------------:|
|  `api_docker_image`  | **Required** |        none        |     Docker image with his tag      |
| `front_docker_image` | **Required** |        none        |     Docker image with his tag      |
|  `ssl_certificate`   | **Optional** |        none        |       Arn of ssl certificate       |
| `mongo_docker_image` | **Optional** | mongo-pipeline:1.0 |     Docker image with his tag      |
|   `ssh_allowed_ip`   | **Optional** |     0.0.0.1/32     | Ip allowed to ssh connexion in ec2 |

- Outputs

```ini
elb_url="elb dns name"
```

##  Zoom in `prod`

#### Global informations

`prod` environment is 1 stack environment

```text
.environments/prod/
```

#### Details prod env



- **ecs stack** ecs stack workdir: `./`
- S3 **backend** `key="file/path"` statically configured (but can be changed with **-backend-config**)
- Variables

|       Variable       |   Required   |      Default       |         Exemple:          |
|:--------------------:|:------------:|:------------------:|:-------------------------:|
|   `vpc_cidr_block`   | **Optional** |    10.0.0.0/16     |      VPCI cidr block      |
|  `api_docker_image`  | **Required** |        none        | Docker image with his tag |
| `front_docker_image` | **Required** |        none        | Docker image with his tag |
| `mongo_docker_image` | **Optional** | mongo-pipeline:1.0 | Docker image with his tag |

- Outputs 

```ini
vpc_id="Main vcp id"
public_subnet_ids="list of subnet ids"
elb_url="elb dns name"
```




# DOCKER


##  Automatic build commands

This command **build** image and **push** it on docker registry
```bash
make build-prod <tag>
make build-staging <tag>
```

##  Manual build commands

Docker **build** arguments (**ARGS**)

```Dockerfile
#Dockerfile
#Terraform environment
ARG ENVIRONMENT=(staging|prod|dev)
```

```bash
docker build ./ --rm -t enzo13/terraform-pipeline-<env>:<tag> --build-arg ENVIRONMENT=<env>
```

##  Docker image workdir

The default docker image working directory is `/terraform/environments/<env>`
```text
/terraform
├─ environments
│  ├─ <env>
├─ modules/
```
