ifeq (init,$(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif

ifeq (plan,$(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif

ifeq (apply,$(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif

ifeq (destroy,$(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif

ifeq (build,$(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif


#==============EDIT VARIABLES HERE=========================
GLOBAL_PROJECT_NAME=who-am-i
DOCKER_REGISTRY=registry.gitlab.com/projects-and-chill/who-am-i/terraform-who-am-i
#==========================================================




#--------------------TERRAFORM RUN COMMANDS--------------------------

EVENT_EMITTER=computer

BACKEND_STAGING_NETWORK=$(GLOBAL_PROJECT_NAME)/$(EVENT_EMITTER)/staging/networking/terraform.tfstate
VARS_STAGING_NETWORK = -var="event_emitter=$(EVENT_EMITTER)"  -var="environment=staging"

BACKEND_STAGING_ECS=$(GLOBAL_PROJECT_NAME)/$(EVENT_EMITTER)/staging/ecs/terraform.tfstate
VARS_STAGING_ECS = -var="event_emitter=$(EVENT_EMITTER)"  -var="environment=staging" -var="s3_remote_state_path=$(BACKEND_STAGING_NETWORK)"

BACKEND_PROD=$(GLOBAL_PROJECT_NAME)/$(EVENT_EMITTER)/production/global/terraform.tfstate
VARS_PROD = -var="event_emitter=$(EVENT_EMITTER)"  -var="environment=production"



cmp: #commit merge and push all
	git add --all && git commit -m"edit"
	git checkout production && git merge master
	git push --all
	git checkout master





.ONESHELL:
.PHONY: init
init:
	if [ $(RUN_ARGS) = "prod" ]
	then
	  terraform -chdir="./environments/$(RUN_ARGS)" init -backend-config="key=$(BACKEND_PROD)"
	else
	  terraform -chdir="./environments/$(RUN_ARGS)/1-networking-stack" init -backend-config="key=$(BACKEND_STAGING_NETWORK)"
	  terraform -chdir="./environments/$(RUN_ARGS)/2-ecs-stack" init -backend-config="key=$(BACKEND_STAGING_ECS)"
	fi

.ONESHELL:
.PHONY: plan
plan:
	if [ $(RUN_ARGS) = "prod" ]
	then
	  terraform -chdir="./environments/$(RUN_ARGS)" plan $(VARS_PROD)
	else
	  terraform -chdir="./environments/$(RUN_ARGS)/1-networking-stack" plan $(VARS_STAGING_NETWORK)
	  terraform -chdir="./environments/$(RUN_ARGS)/2-ecs-stack" plan $(VARS_STAGING_ECS)
	fi

.ONESHELL:
.PHONY: apply
apply:
	if [ $(RUN_ARGS) = "prod" ]
	then
	  terraform -chdir="./environments/$(RUN_ARGS)" apply -auto-approve $(VARS_PROD)
	else
	  terraform -chdir="./environments/$(RUN_ARGS)/1-networking-stack" apply -auto-approve $(VARS_STAGING_NETWORK)
	  terraform -chdir="./environments/$(RUN_ARGS)/2-ecs-stack" apply -auto-approve  $(VARS_STAGING_ECS)
	fi


.ONESHELL:
.PHONY: destroy
destroy:
	if [ $(RUN_ARGS) = "prod" ]
	then
	  terraform -chdir="./environments/$(RUN_ARGS)" destroy -auto-approve $(VARS_PROD)
	else
	  terraform -chdir="./environments/$(RUN_ARGS)/2-ecs-stack" destroy -auto-approve $(VARS_STAGING_ECS)
	  terraform -chdir="./environments/$(RUN_ARGS)/1-networking-stack" destroy -auto-approve $(VARS_STAGING_NETWORK)
	fi


#-------------------- DOCKERBUILD IMAGE FOR MICROSERVICES CD --------------------------

IMAGE_NAME=$(DOCKER_REGISTRY)

.ONESHELL:
.PHONY: build
build:
	docker build --rm -t $(IMAGE_NAME):latest ./
	[ ! -z  $(RUN_ARGS) ] && docker tag $(IMAGE_NAME):latest $(IMAGE_NAME):$(RUN_ARGS)
	docker push --all-tags $(IMAGE_NAME)


